// handlers
function handleContactFormResult(data) {
	if(data.status.code=='okay') {
		$('#contact #contact_form').slideUp(250);
		$('#contact .message').slideDown(250);
	}
	else {
		$('form button').removeAttr('disabled');
		// todo: notify
	}
}

// Mobile menu
$('.menubutton').click(function() {
	$('header nav').slideToggle('', function() {
	});
});

// Contact form validation
$("#contact_form").validate();

setTimeout(function() {
	$("#contact_form").submit(function(e) {
		e.preventDefault();
		if(!$('input, textarea').hasClass('error')) {
			// prevent multiple submissions
			$('form button').attr('disabled', 'disabled');

			$.post('http://ghosts.localhost:8000/support/contact/form',
				$('#contact_form').serialize(),
				handleContactFormResult,
				'json');
		}
		else {
			return false;
		}
	});
}, 500);

